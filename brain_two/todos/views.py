from django.shortcuts import render
from django.views import generic
from .models import TodoList
from django.urls import reverse_lazy
from .models import TodoItem


# Create your views here.

class TodoListView(generic.ListView):
    model = TodoList
    template_name = 'todos/todo_list.html'
    context_object_name = 'todo_lists'


class TodoListDetailView(generic.DetailView):
    model = TodoList
    template_name = 'todos/todo_list_detail.html'
    context_object_name = 'todo_list'


class TodoListCreateView(generic.CreateView):
    model = TodoList
    fields = ['name']
    template_name = 'todos/todo_list_create.html'
    success_url = reverse_lazy('todo_list_detail')

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'pk': self.object.pk})



class TodoListUpdateView(generic.UpdateView):
    model = TodoList
    fields = ['name']
    template_name = 'todos/todo_list_update.html'
    context_object_name = 'todo_list'

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'pk': self.object.pk})



class TodoListDeleteView(generic.DeleteView):
    model = TodoList
    template_name = 'todos/todo_list_delete.html'
    success_url = reverse_lazy('todo_list_list')


class TodoItemCreateView(generic.CreateView):
    model = TodoItem
    fields = ['task', 'due_date', 'is_completed', 'list']
    template_name = 'todos/todo_item_create.html'

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'pk': self.object.list.pk})



class TodoItemUpdateView(generic.UpdateView):
    model = TodoItem
    fields = ['task', 'due_date', 'is_completed', 'list']
    template_name = 'todos/todo_item_update.html'

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'pk': self.object.list.pk})
